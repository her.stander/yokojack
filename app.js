const axios = require('axios');
const Game = require('./game');

// URL for the json test/game object
const testFileUrl = 'https://s3-eu-west-1.amazonaws.com/yoco-testing/tests.json'

function evaluateGames(data) {
    data.forEach(deal => {
        // for each delt decks, create a game and test the results
        let game = new Game(deal);
        game.test();
    })
}

// Download the data and asign it to a variable then test the delt cards
async function downloadAndTest(url) {
    try {
        const response = await axios.get(url);
        const data = response.data;
        // Ocne the data is asigned, we can play the game
        evaluateGames(data)
    } catch (error) {
        console.log(error);
        return null;
    }
}

// Kick of the download and Test
downloadAndTest(testFileUrl);