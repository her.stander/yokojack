module.exports = class Game {
    constructor(deal) {
        this.cardPointNumberPattern = /\d+/g;
        this.playerADeck = deal.playerA;
        this.playerBDeck = deal.playerB;
        this.expectedOutcome = deal.playerAWins;
        this.playerAWins;
    }

    rankSuit(suit) {
        switch (suit) {
            case 'S':
                return 4
            case 'H':
                return 3
            case 'C':
                return 2
            case 'D':
                return 1
        }
    }

    calculateDeckScore(player) {
        let results = []
        let total = 0;
        let highCard = 0;
        let highCardSuit = 0;
        player.forEach(card => {

            let cardValue = card.match(this.cardPointNumberPattern);
            let cardSuit = card[card.length - 1]


            if (cardValue === null) {
                if (['J', 'Q', 'K',].includes(card[0])) {
                    total += 10
                    switch (card[0]) {
                        case 'J':
                            if (highCard <= 10.1) {
                                highCard = 10.1
                                highCardSuit = this.rankSuit(cardSuit)
                            }
                            
                        case 'Q':
                            if (highCard <= 10.2) {
                                highCard = 10.2
                                highCardSuit = this.rankSuit(cardSuit)
                            }
                        case 'K':
                            if (highCard <= 10.3) {
                                highCard = 10.3
                                highCardSuit = this.rankSuit(cardSuit)
                            }
                            
                    }
                } else if (card[0] == "A") {
                    if (highCard <= 11) {
                        highCard = 11
                        highCardSuit = this.rankSuit(cardSuit)
                    }
                    total += 11

                }
            } else {
                if (highCard <= parseInt(cardValue[0])) {
                    highCard = parseInt(cardValue[0])
                    highCardSuit = this.rankSuit(cardSuit)
                }
                total += parseInt(cardValue[0])
            }
        });
        // console.log(highCard, total)
        results['total'] = total;
        results['highCard'] = highCard;
        results['highCardSuit'] = highCardSuit;
        return results;
    }

    test() {
        let playerA = this.calculateDeckScore(this.playerADeck);
        let playerB = this.calculateDeckScore(this.playerBDeck);

        let playerATotal = playerA.total;
        let playerAHighCard = playerA.highCard;
        let playerAHighCardSuit = playerA.highCardSuit;

        let playerBTotal = playerB.total;
        let playerBHighCard = playerB.highCard;
        let playerBHighCardSuit = playerB.highCardSuit;


        breakme: {
            if (playerBTotal > 21) {
                this.playerAWins = true
                break breakme;
            } else if (playerATotal > 21) {
                this.playerAWins = false
                break breakme;
            } else if (playerATotal > playerBTotal) {
                this.playerAWins = true
                break breakme;
            } else if (playerATotal < playerBTotal) {
                this.playerAWins = false
                break breakme;
            } else if (playerATotal === playerBTotal) {
                this.playerAWins = playerAHighCard > playerBHighCard ? true : false
                if (playerAHighCard === playerBHighCard) {
                    this.playerAWins = playerAHighCardSuit > playerBHighCardSuit ? true : false
                }
            }
        }
        
        console.log({
            'Expected outcome': this.expectedOutcome,
            'Test passed': this.playerAWins === this.expectedOutcome,
            'Winner': this.playerAWins ? 'Player A' : 'Player B'
        })

        if (this.playerAWins !== this.expectedOutcome) {
            console.error('Test failed!')
        }
    }
}
